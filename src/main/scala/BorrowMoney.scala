trait BorrowMoney {
  this: GreatHouse =>
  private val random = new scala.util.Random

  def borrowMoney(): Wealth = {

    HardWorkerWealth(moneyAmount = wealth.moneyAmount + random.nextInt(1337), armyPower = wealth.armyPower)
  }
}
