trait MakeWildFire {
  this: GreatHouse =>
  def makeWildFire(): Wealth = {
    HardWorkerWealth(moneyAmount = wealth.moneyAmount, armyPower = wealth.armyPower + 42)
  }
}
