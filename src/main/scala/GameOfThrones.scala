class GameOfThrones(val lannisters: Lannisters, val targaryen: Targaryen, val step: Int = 0) {
  def nextTurn(lannistersStrategy: Strategy[Lannisters])(targaryenStrategy: Strategy[Targaryen]): GameOfThrones = {
    var newLannisters = lannisters
    var newTargaryen = targaryen
    lannistersStrategy match {
      case lannistersForeignStrategy: ForeignStrategy[Lannisters, Targaryen] =>
        val (newLannistersWealth, newTargaryenWealth) = lannistersForeignStrategy.run(newLannisters, newTargaryen)
        newLannisters = Lannisters(initialWealth = newLannistersWealth)
        newTargaryen = Targaryen(initialWealth = newTargaryenWealth)
      case lannistersDomesticStrategy: DomesticStrategy[Lannisters] =>
        val newLannistersWealth = lannistersDomesticStrategy.run(newLannisters)
        newLannisters = Lannisters(initialWealth = newLannistersWealth)
    }

    targaryenStrategy match {
      case targaryenForeignStrategy: ForeignStrategy[Targaryen, Lannisters] =>
        val (newTargaryenWealth, newLannistersWealth) = targaryenForeignStrategy.run(newTargaryen, newLannisters)
        newTargaryen = Targaryen(initialWealth = newTargaryenWealth)
        newLannisters = Lannisters(initialWealth = newLannistersWealth)
      case targaryenDomesticStrategy: DomesticStrategy[Targaryen] =>
        val newTargaryenWealth = targaryenDomesticStrategy.run(newTargaryen)
        newTargaryen = Targaryen(initialWealth = newTargaryenWealth)
    }

    new GameOfThrones(
      lannisters = newLannisters,
      targaryen = newTargaryen,
      step = step + 1
    )
  }
}
