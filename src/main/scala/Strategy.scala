abstract class Strategy[T](function: T => Wealth) {
  def run(greatHouse: T): Wealth = {
    function(greatHouse)
  }
}
