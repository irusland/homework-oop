case class ForeignStrategy[TDomestic <: GreatHouse, TForeign](
  function: TDomestic => TForeign => (Wealth, Wealth)
) extends Strategy[TDomestic](_ => HardWorkerWealth()) {
  def run(domesticHouse: TDomestic, foreignHouse: TForeign): (Wealth, Wealth) = {
    function(domesticHouse)(foreignHouse)
  }
}
