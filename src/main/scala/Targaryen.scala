case class Targaryen(initialWealth: Wealth) extends GreatHouse with MakeWildFire with BorrowMoney with CallDragon with Attack[Targaryen, GreatHouse] {
  override val name: String = "Targaryen"
  override val wealth: Wealth = initialWealth

}
