trait Attack[TDomestic <: GreatHouse, TForeign <: GreatHouse] {
  this: GreatHouse =>
  private val random = new scala.util.Random

  def attack(foreign: TForeign): (Wealth, Wealth) = {
    val amountStolen = random.nextInt(foreign.wealth.moneyAmount)
    val troopsLost = math.min(wealth.armyPower, foreign.wealth.armyPower)
    (
      HardWorkerWealth(moneyAmount = wealth.moneyAmount + amountStolen, armyPower = wealth.armyPower - troopsLost),
      HardWorkerWealth(moneyAmount = foreign.wealth.moneyAmount - amountStolen, armyPower = foreign.wealth.armyPower - troopsLost)
    )
  }
}
