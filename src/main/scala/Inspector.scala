class Inspector {
  def printGameState(gameOfThrones: GameOfThrones): Unit = {
    println(s"[${gameOfThrones.step}]")
    for (house <- List(gameOfThrones.lannisters, gameOfThrones.targaryen)) {
      println(s"    ${house.name} = ${house.wealth.moneyAmount}⚒︎, ${house.wealth.armyPower}⚔︎")
    }
  }
}
