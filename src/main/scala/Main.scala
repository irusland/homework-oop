object Main {
  def main(args: Array[String]): Unit = {
    val lannisters = Lannisters(initialWealth = HardWorkerWealth())
    val targaryen = Targaryen(initialWealth = HardWorkerWealth())
    var game = new GameOfThrones(lannisters = lannisters, targaryen = targaryen)
    val inspector = new Inspector()

    val lannistersStrateries: List[Strategy[Lannisters]] = List(
      new DomesticStrategy[Lannisters](house => house.borrowMoney()),
      new DomesticStrategy[Lannisters](house => house.borrowMoney()),
      new ForeignStrategy[Lannisters, Targaryen](domestic => foreigners => domestic.attack(foreigners)),
    )
    val targaryenStrategies: List[Strategy[Targaryen]] = List(
      new DomesticStrategy[Targaryen](house => house.makeWildFire()),
      new DomesticStrategy[Targaryen](house => house.callDragon()),
      new ForeignStrategy[Targaryen, Lannisters](domestic => foreigners => domestic.attack(foreigners)),
    )

    inspector.printGameState(game)
    (lannistersStrateries zip targaryenStrategies).foreach(strategyPair => {
      val lannistersStrategy = strategyPair._1
      val targaryenStrategy = strategyPair._2

      game = game.nextTurn(lannistersStrategy)(targaryenStrategy)
      inspector.printGameState(game)
    }
    )
  }
}