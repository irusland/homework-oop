trait CallDragon {
  this: GreatHouse =>

  def callDragon(): Wealth = {
    HardWorkerWealth(moneyAmount = wealth.moneyAmount, armyPower = wealth.armyPower * 2)
  }
}
