case class Lannisters(initialWealth: Wealth) extends GreatHouse with CallDragon with MakeWildFire with BorrowMoney with Attack[Lannisters, GreatHouse] {
  override val name: String = "Lannisters"
  override val wealth: Wealth = initialWealth
}
